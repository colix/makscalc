<?php


class Calc {

// Цены
    const  Okna = 787;
    const  Podshiv = 1800;
    const  Dobr = 20000;
    const  Derevo = 7700;
    const  Ytepl = 78;
    const  Krovlya = 335;
    const  Saiding = 400;
    const  Imitaciya = 310;
    const  Montazh = 500;
    const  Yteplenie = 170;
    const  Otdelka = 300;
    const  Obshiv = 300;
    const  Dopytepl = 200;
    const  Chpol = 300;
    const  D50 = 0.009;
    const  D100 = 0.018;
    const  D150 = 0.027;
    const  D200 = 0.036;
    const  Stropilo = 0.025;
    const  Dver = 20000;
    const  Dverv = 655;
    const  Metiz = 370;
    const  Pokraska = 250;
    const  Obresh = 0.013;
    const  VZ = 0.005;
    const  Fund = 1800;
    const  QD = 1050;

    public function Calculate($data) {
	$floors = $data['floors'];
	$attic = $data['attic'];
	$length = $data['l'];
	$width = $data['w'];
	$height = $data['h'];
	$angle = $data['angle'];
	$season = $data['season'];

	$Sz = $length * $width;
	$Sp = $Sz * ($floors + $attic);
	$Lks = $width/(2 * cos($angle * 3.14/180));
	$Ls = $Lks + 0.5 * ($floors + $attic);
	$Sk = ($length + 1) * $Ls * 2;
	$Ss = ($length + $width) * $height * 2 * ($floors + $attic/2);
	$Sf = 0.5 * $Lks * $width * sin($angle * 3.14/180);
	$So = ($length + $width) * 2 * ($floors + $attic) * 0.2 + $Ss + $Sf * 2;
	$P = $Sp * 0.3;
	$Spr = $P * $height;
	$Sos = $Ss + $Sf * 2 + $Spr * 2;
	$Sy = (($Sp + $Ss) + ($Sf * 2 * $attic) + ($Sz * 0.25 * $attic)) * $season/50 + ($Spr * 2);
	$Spt = ($Sz * $floors) + ($attic * $Sz * 1.25);
	$Lp = ($width + 1) * 2 + $Ls * 4;
	$Od = self::Dver + $Sp * self::Okna;

	$complect = ($Ss * self::D150 + $P * $height * self::D100 + $Sz * ($floors + 1) * self::D200 + $Sk * self::Stropilo + $Sf * 2 * self::D150) * self::Dobr + $Sz * self::Metiz + $attic * $Sz * self::Metiz/2;
	$fullhouse = $complect + ($Ss + $P * $height + $Sz * ($floors + 1) * self::Montazh + $Sk * self::Krovlya + $Ss * self::Montazh *2 + $Sk * (self::VZ + self::Obresh) * self::Derevo) + ($Sz * self::Fund) +
		      (self::Yteplenie * $Sy + $Ss * self::Okna + self::Dver + $Spr * self::Dverv + ($Sos + $So) * self::VZ * self::Derevo) +
		      ($So * (self::Saiding + self::Obshiv) + $Lp * self::Podshiv + $So * self::VZ * self::Derevo) + (($So + $Spt) * (self::Imitaciya + self::Otdelka) +
		      $Sp * (self::Otdelka + self::Chpol)) + ($Sp * self::QD) + (100000 * ($floors - 1 + $attic)) +(20000 * $length) + (($So + $Sos + $Spt) * self::Pokraska);

	$result = array();
	$result['complect'] = $complect;
	$result['fullhouse'] = $fullhouse;
	return json_encode($result);
    }

}

if (isset($_POST)) {

$data = $_POST;
$myCalc = new Calc();
echo $myCalc->Calculate($data);
}